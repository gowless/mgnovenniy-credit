package com.mgnovenniycredit.activities;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustConfig;
import com.adjust.sdk.LogLevel;
import com.facebook.applinks.AppLinkData;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.mgnovenniycredit.MainClass;
import com.mgnovenniycredit.R;
import com.mgnovenniycredit.models.post.get.Data;
import com.mgnovenniycredit.models.post.get.Liste;
import com.mgnovenniycredit.network.Initializator;
import com.mgnovenniycredit.network.Interface;
import java.util.Iterator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends AppCompatActivity {

    //attribution strings
    public static String  net, cam, adg, cre;



    //https://api.mocki.io/v1/fe341b7d
    //url base
    public static  SharedPreferences settings;
    public static  SharedPreferences.Editor editor;

    //carrier name string
    String carrier;


    //static AD_ID
    public static String ad_id;

    @SuppressLint("CommitPrefEdits")
    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        settings = getSharedPreferences("LOCAL", Context.MODE_PRIVATE);
        editor = settings.edit();

        //get ad_id
        getID();

        //getting carrier ISO name
        getCarrier();

        //checking carrier to match current country code SIM
        if (carrier.equals("ru")) {
            getJsonData();
        } else {
            getJsonDataCloak();
        }

    }


    //starting CloakActivity
    public void getCloak() {
        startActivity(new Intent(SplashActivity.this, CloakActivity.class));
    }

    //get carrier name
    public void getCarrier() {
        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        carrier = manager.getSimCountryIso();
    }

    //starting BeforeMain activity
    public void getBeforeMain() {
        startActivity(new Intent(SplashActivity.this, BeforeMainActivity.class));
    }

    //getting advertising ID
    public void getID() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(SplashActivity.this);
                    ad_id = adInfo != null ? adInfo.getId() : null;

                    assert ad_id != null;

                } catch (Exception ignored) {

                }
            }
        });
    }

    //setting to get json file and parse it to models in main case
    public void getJsonData() {
        Interface apiInterfaceCount = Initializator.getClient().create(Interface.class);
        Call<Data> call = apiInterfaceCount.getData(MainClass.REGION, MainClass.APPID);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                assert response.body() != null;
                MainClass.listDataAll = response.body().getList();
                MainClass.isEmpty = response.body().getCategories().isEmpty();
                MainClass.numberOfTabs = response.body().getCategories().size();
                MainClass.listDataBad = response.body().getList();
                MainClass.listDataZero = response.body().getList();

                //iterator for 2-nd tab (new list)
                for (Liste s : MainClass.listDataZero) {
                    if (s.getCategories().contains("zero")) {
                        MainClass.testDataList.add(s);
                    }

                }


                //iterator for 3-rd tab (bad list)
                for (Liste m : MainClass.listDataBad) {
                    if (m.getCategories().contains("badCreditHistory")) {
                        MainClass.testDataList2.add(m);
                    }

                }


                //switching between numbers of tabs (maximum - 3)
                switch (MainClass.numberOfTabs) {
                    case 0:
                        break;
                    case 1:
                        MainClass.first = response.body().getCategories().get(0).getLabel();
                        break;
                    case 2:
                        MainClass.first = response.body().getCategories().get(0).getLabel();
                        MainClass.second = response.body().getCategories().get(1).getLabel();
                        break;
                    case 3:
                        MainClass.first = response.body().getCategories().get(0).getLabel();
                        MainClass.second = response.body().getCategories().get(1).getLabel();
                        MainClass.third = response.body().getCategories().get(2).getLabel();
                        break;
                }

                if (settings.getInt("firstlaunch", 0) == 1){
                    //open MainActivity
                    getBeforeMain();
                } else {
                    adjustGetData();
                }

            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                //starting MainActivity when failed to connect
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            }
        });

    }



    //setting to get json file and parse it to models in case of cloak
     public void getJsonDataCloak(){
         Interface apiInterfaceCount = Initializator.getClient().create(Interface.class);
         Call<Data> call = apiInterfaceCount.getData(MainClass.REGION, MainClass.APPID);
         call.enqueue(new Callback<Data>() {
             @Override
             public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                 assert response.body() != null;
                 MainClass.listDataAll = response.body().getList();
                 MainClass.isEmpty = response.body().getCategories().isEmpty();
                 MainClass.numberOfTabs = response.body().getCategories().size();
                 MainClass.listDataBad = response.body().getList();
                 MainClass.listDataZero = response.body().getList();

                 //iterator for 2-nd tab (new list)
                 Iterator<Liste> x = MainClass.listDataZero.iterator();
                 while (x.hasNext()){
                     Liste s = x.next();
                     if (s.getCategories().contains("zero")){
                         MainClass.testDataList.add(s);
                     } else {
                         //x.remove();
                     }
                 }


                 //iterator for 3-rd tab (bad list)
                 Iterator<Liste> i = MainClass.listDataBad.iterator();
                 while (i.hasNext()){
                     Liste m = i.next();
                     if (m.getCategories().contains("badCreditHistory")){
                         MainClass.testDataList2.add(m);

                     } else {
                         //  i.remove();
                     }
                 }

                 //switching between numbers of tabs (maximum - 3)
                 switch (MainClass.numberOfTabs) {
                     case 0:
                         break;
                     case 1:
                         MainClass.first = response.body().getCategories().get(0).getLabel();
                         break;
                     case 2:
                         MainClass.first = response.body().getCategories().get(0).getLabel();
                         MainClass.second = response.body().getCategories().get(1).getLabel();
                         break;
                     case 3:
                         MainClass.first = response.body().getCategories().get(0).getLabel();
                         MainClass.second = response.body().getCategories().get(1).getLabel();
                         MainClass.third = response.body().getCategories().get(2).getLabel();
                         break;
                 }
                 //open cloak
               getCloak();
             }

             @Override
             public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                 //starting MainActivity when failed to connect
                 startActivity(new Intent(SplashActivity.this, MainActivity.class));
             }
         });

     }

    //getting fb deepLinkdata
    @SuppressLint("ApplySharedPref")
    public void getDeepLink(){
        try {
            AppLinkData.fetchDeferredAppLinkData(getApplicationContext(), appLinkData -> {
                if (appLinkData == null || appLinkData.getTargetUri() == null) {
                    editor.putString("sub1", "failedFBDeepLink");
                    editor.putString("sub2", "failedFBDeepLink");
                    editor.putString("sub3", "failedFBDeepLink");
                    editor.commit();
                } else {
                    editor.putString("sub1", appLinkData.getTargetUri().getQueryParameter("sub1"));
                    Log.d("deeplink", appLinkData.getTargetUri().getQueryParameter("sub1"));
                    editor.putString("sub2", appLinkData.getTargetUri().getQueryParameter("sub2"));
                    editor.putString("sub3", appLinkData.getTargetUri().getQueryParameter("sub3"));
                    editor.commit();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //getting adjust data
    private void adjustGetData(){
        String appToken = "yebqteksfvnk";
        String environment = AdjustConfig.ENVIRONMENT_PRODUCTION;
        AdjustConfig config = new AdjustConfig(this, appToken, environment);
        // enable all logs
        config.setLogLevel(LogLevel.VERBOSE);
        Adjust.onCreate(config);
        Adjust.onResume();
        MainClass.font = getResources().getConfiguration().fontScale;
        config.setOnAttributionChangedListener(attribution -> {
            MainClass.trackerToken = attribution.trackerToken;
            MainClass.trackerName = attribution.trackerName;
            MainClass.network = attribution.network;

            //checking on if/else statements in case of organic install
            if (attribution.campaign == null || attribution.adgroup == null || attribution.creative == null){
                MainClass.campaign = "organic";
                MainClass.adgroup = "organic";
                MainClass.creative = "organic";
            } else {
                MainClass.campaign = attribution.campaign;
                MainClass.adgroup = attribution.adgroup;
                MainClass.creative = attribution.creative;
            }
            MainClass. adid = attribution.adid;


            //put to sharedprefs
            editor.putString("trackerToken", MainClass.trackerToken);
            Log.d("adjust", MainClass.network);
            editor.putString("trackerName", MainClass.trackerName);
            editor.putString("network", MainClass.network);
            editor.putString("campaign", MainClass.campaign);
            editor.putString("adgroup", MainClass.adgroup);
            editor.putString("creative", MainClass.creative);
            editor.putString("adid", MainClass.adid);
            editor.apply();

            if (settings.getString("network", "").equals("Facebook Installs")){
                //getting fb subid's
                getDeepLink();
            }


            if (settings.getInt("firstlaunch", 0) == 1){

            } else {
                //additional cases od deeplink subid's
                if (settings.getString("network", "").equals("Facebook Installs")){

                } else if (settings.getString("network", "").equals("Google Ads UAC")){
                    editor.putString("sub1", "uac");
                    editor.putString("sub2", "uac");
                    editor.putString("sub3", "uac");
                } else if (settings.getString("network", "").equals("Organic")){
                    editor.putString("sub1", "organic");
                    editor.putString("sub2", "organic");
                    editor.putString("sub3", "organic");
                } else if (settings.getString("network", "").equals("Unattributed")){
                    editor.putString("sub1", "unattributed");
                    editor.putString("sub2", "unattributed");
                    editor.putString("sub3", "unattributed");
                }
            }

            if (settings.getInt("firstlaunch", 0) == 1){

            } else {
                editor.putInt("firstlaunch", 1);
            }
            editor.apply();

            getBeforeMain();
        });
    }

}