package com.mgnovenniycredit.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.mgnovenniycredit.R;


public class InfoDetailsActivity extends AppCompatActivity {

    ImageView backArrow;
    //init toolbar
    Toolbar toolbar;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_details);
        backArrow = findViewById(R.id.img_back_arrow);

        backArrow.setOnClickListener(v -> {
            startActivity(new Intent(InfoDetailsActivity.this, CloakActivity.class));
        });
        setSupportActionBar(toolbar);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(InfoDetailsActivity.this, CloakActivity.class));
    }
}